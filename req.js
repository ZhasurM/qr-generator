try {
    Zabbix.log(4, '[ POST QR webhook ] Started with params: ' + value);

    var result = {
            'tags': {
                'endpoint': 'post'
            }
        },
        params = JSON.parse(value),
        req = new HttpRequest(),
        fields = {},
        resp;

    if (params.HTTPProxy) {
        req.setProxy(params.HTTPProxy);
    }

    req.addHeader('Content-Type: application/json');
    //req.addHeader('Authorization: Basic ' + params.authentication);

    fields.alert_subject = params.alert_subject;
    fields.event_recovery_value = params.event_recovery_value;
    fields.event_source = params.event_source;
    fields.event_time = params.event_time;
    fields.event_date = params.event_date;
    fields.host = params.host;
    fields.operational_data = params.operational_data;
    fields.problem_id = params.problem_id;

    resp = req.post(params.URL,
        JSON.stringify({"fields": fields})
    );

    if (req.getStatus() != 200) {
        throw 'Response code: ' + req.getStatus();
    }

    resp = JSON.parse(resp);
    result.tags.issue_id = resp.id;
    result.tags.issue_key = resp.key;

    return JSON.stringify(result);
}
catch (error) {
    Zabbix.log(4, '[ QR webhook ] Issue creation failed json : ' + JSON.stringify({"fields": fields}));
    Zabbix.log(3, '[ QR webhook ] issue creation failed : ' + error);

    throw 'Failed with error: ' + error;
}