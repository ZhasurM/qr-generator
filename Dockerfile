FROM python:3

WORKDIR /usr/src/app/

COPY . /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

RUN export QUEUE_MAXSIZE=100

EXPOSE 5000

CMD ["python", "app.py"]