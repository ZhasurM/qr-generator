# save this as app.py
from flask import Flask, request, redirect, url_for
from qr import create_qr
import os
from queue import Queue
from flask import render_template, request

app = Flask(__name__)

q = Queue(maxsize=os.getenv('QUEUE_MAXSIZE'))

@app.route("/", methods=['GET'])
def hello():
    with open('sec.txt', 'r') as file:
        sec = file.read()
    if request.method == 'GET':
        if q.empty() == True:
            return render_template('empty.html', sec=sec)
        else:
            data = q.get()
            return render_template('index.html', data=data, sec=sec)


@app.route("/restart", methods=['POST'])
def restart():
    if request.method == 'POST':
        sec = request.form.get('delay')
        with open('sec.txt', 'w') as file:
            # Записываем данные в конец файл
            file.write(sec)
        return redirect(url_for('hello'))







@app.route("/qr_api", methods=['POST', 'GET'])
def qr():
    if request.method == 'POST':
        msg = request.get_json()
        qr_name = create_qr(msg)
        msg['qr_name'] = qr_name
        q.put(msg)
        print(msg)
        print(q.qsize())
        if os.path.exists('./static/log.txt'):
            with open('./static/log.txt', 'a') as file:
                # Записываем данные в конец файл
                file.write(str(msg)+'\n')
        else:
            with open('./static/log.txt', 'w') as file:
                # Записываем данные в файл
                file.write(str(msg)+'\n')
        return msg
    else:
        return 'Hello'

app.run(host='0.0.0.0', port=5000)