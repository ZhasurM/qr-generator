import qrcode
import datetime
import os
import json

def create_qr(data):
    # Создание объекта QR-кода
    qr = qrcode.QRCode(version=1, box_size=20, border=5)
    # Добавление данных в QR-код
    qr.add_data(data)
    qr.make(fit=True)
    # Создание изображения QR-кода
    img = qr.make_image(fill_color="black", back_color="white")
    # Сохранение изображения в файл
    now = datetime.datetime.now()
    # Форматирование даты и времени
    formatted_datetime = now.strftime("%Y-%m-%d_%H:%M:%S")
    img.save(f"./static/{formatted_datetime}.png")
    return f'{formatted_datetime}.png'
