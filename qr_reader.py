import cv2
from pyzbar.pyzbar import decode

# загружаем изображение
img = cv2.imread('Downloads/photo_2023-04-30_20-18-04.jpg')

# декодируем QR-код
decoded_data = decode(img)

# выводим результат
for obj in decoded_data:
    print('QR Code data:', obj.data.decode('utf-8'))